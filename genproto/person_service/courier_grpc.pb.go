// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: courier.proto

package person_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CourierServiceClient is the client API for CourierService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CourierServiceClient interface {
	// Sends a greeting
	Create(ctx context.Context, in *CreateCourier, opts ...grpc.CallOption) (*IdReqRes, error)
	Update(ctx context.Context, in *Courier, opts ...grpc.CallOption) (*ResponseString, error)
	Get(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*Courier, error)
	Delete(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*ResponseString, error)
	GetAll(ctx context.Context, in *GetAllCourierRequest, opts ...grpc.CallOption) (*GetAllCourierResponse, error)
}

type courierServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCourierServiceClient(cc grpc.ClientConnInterface) CourierServiceClient {
	return &courierServiceClient{cc}
}

func (c *courierServiceClient) Create(ctx context.Context, in *CreateCourier, opts ...grpc.CallOption) (*IdReqRes, error) {
	out := new(IdReqRes)
	err := c.cc.Invoke(ctx, "/person_service.CourierService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) Update(ctx context.Context, in *Courier, opts ...grpc.CallOption) (*ResponseString, error) {
	out := new(ResponseString)
	err := c.cc.Invoke(ctx, "/person_service.CourierService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) Get(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*Courier, error) {
	out := new(Courier)
	err := c.cc.Invoke(ctx, "/person_service.CourierService/Get", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) Delete(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*ResponseString, error) {
	out := new(ResponseString)
	err := c.cc.Invoke(ctx, "/person_service.CourierService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) GetAll(ctx context.Context, in *GetAllCourierRequest, opts ...grpc.CallOption) (*GetAllCourierResponse, error) {
	out := new(GetAllCourierResponse)
	err := c.cc.Invoke(ctx, "/person_service.CourierService/GetAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CourierServiceServer is the server API for CourierService service.
// All implementations should embed UnimplementedCourierServiceServer
// for forward compatibility
type CourierServiceServer interface {
	// Sends a greeting
	Create(context.Context, *CreateCourier) (*IdReqRes, error)
	Update(context.Context, *Courier) (*ResponseString, error)
	Get(context.Context, *IdReqRes) (*Courier, error)
	Delete(context.Context, *IdReqRes) (*ResponseString, error)
	GetAll(context.Context, *GetAllCourierRequest) (*GetAllCourierResponse, error)
}

// UnimplementedCourierServiceServer should be embedded to have forward compatible implementations.
type UnimplementedCourierServiceServer struct {
}

func (UnimplementedCourierServiceServer) Create(context.Context, *CreateCourier) (*IdReqRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedCourierServiceServer) Update(context.Context, *Courier) (*ResponseString, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedCourierServiceServer) Get(context.Context, *IdReqRes) (*Courier, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Get not implemented")
}
func (UnimplementedCourierServiceServer) Delete(context.Context, *IdReqRes) (*ResponseString, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedCourierServiceServer) GetAll(context.Context, *GetAllCourierRequest) (*GetAllCourierResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}

// UnsafeCourierServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CourierServiceServer will
// result in compilation errors.
type UnsafeCourierServiceServer interface {
	mustEmbedUnimplementedCourierServiceServer()
}

func RegisterCourierServiceServer(s grpc.ServiceRegistrar, srv CourierServiceServer) {
	s.RegisterService(&CourierService_ServiceDesc, srv)
}

func _CourierService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateCourier)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.CourierService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Create(ctx, req.(*CreateCourier))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Courier)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.CourierService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Update(ctx, req.(*Courier))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_Get_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdReqRes)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Get(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.CourierService/Get",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Get(ctx, req.(*IdReqRes))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdReqRes)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.CourierService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Delete(ctx, req.(*IdReqRes))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAllCourierRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.CourierService/GetAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).GetAll(ctx, req.(*GetAllCourierRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// CourierService_ServiceDesc is the grpc.ServiceDesc for CourierService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CourierService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "person_service.CourierService",
	HandlerType: (*CourierServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _CourierService_Create_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _CourierService_Update_Handler,
		},
		{
			MethodName: "Get",
			Handler:    _CourierService_Get_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _CourierService_Delete_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _CourierService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "courier.proto",
}
