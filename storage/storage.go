package storage

import (
	"context"
	"order/genproto/order_service"
)

type StoregeI interface {
	Order() OrderI
	OrderProduct() OrderProductI
	DeliveryTarif() DeliveryTarifI
	AlternativeTarif() AlternativeTarifI
}

type OrderI interface {
	Create(context.Context, *order_service.CreateOrder) (string, error)
	Update(context.Context, *order_service.Order) (string, error)
	Get(context.Context, *order_service.IdReqRes) (*order_service.Order, error)
	GetAll(context.Context, *order_service.GetAllOrderRequest) (*order_service.GetAllOrderResponse, error)
	Delete(context.Context, *order_service.IdReqRes) (string, error)
	UpdateStatus(context.Context, *order_service.UpdateOrderStatus) (string, error)
	GetAllNotCourierAcceptedOrder(context.Context, *order_service.PageLimit) (*order_service.GetAllOrderResponse, error)
	GetAllByCourierId(context.Context, *order_service.CourierIdRequest) (*order_service.GetAllOrderResponse, error)
}

type OrderProductI interface {
	Create(context.Context, *order_service.CreateOrderProduct) (string, error)
	Update(context.Context, *order_service.OrderProduct) (string, error)
	Get(context.Context, *order_service.IdReqRes) (*order_service.OrderProduct, error)
	GetAll(context.Context, *order_service.GetAllOrderProductRequest) (*order_service.GetAllOrderProductResponse, error)
	Delete(context.Context, *order_service.IdReqRes) (string, error)
}

type DeliveryTarifI interface {
	Create(context.Context, *order_service.CreateDeliveryTarif) (string, error)
	Update(context.Context, *order_service.DeliveryTarif) (string, error)
	Get(context.Context, *order_service.IdReqRes) (*order_service.DeliveryTarif, error)
	GetAll(context.Context, *order_service.GetAllDeliveryTarifRequest) (*order_service.GetAllDeliveryTarifResponse, error)
	Delete(context.Context, *order_service.IdReqRes) (string, error)
}

type AlternativeTarifI interface {
	Create(context.Context, *order_service.CreateAlternativeTarif) (string, error)
	Update(context.Context, *order_service.AlternativeTarif) (string, error)
	Get(context.Context, *order_service.IdReqRes) (*order_service.AlternativeTarif, error)
	GetAll(context.Context, *order_service.GetAllAlternativeTarifRequest) (*order_service.GetAllAlternativeTarifResponse, error)
	Delete(context.Context, *order_service.IdReqRes) (string, error)
}
