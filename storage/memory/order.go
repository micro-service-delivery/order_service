package memory

import (
	"context"
	"fmt"
	"order/genproto/order_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type orderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) *orderRepo {

	return &orderRepo{
		db: db,
	}

}

func (c *orderRepo) Create(ctx context.Context, req *order_service.CreateOrder) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
		orders (id,order_id,client_id,branch_id,courier_id,type,address,delivery_price,price,discount,payment_type,status)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)`

	_, err := c.db.Exec(ctx, query,
		id,
		req.OrderId,
		req.ClientId,
		req.BranchId,
		req.CourierId,
		req.Type,
		req.Address,
		req.DeliveryPrice,
		req.Price,
		req.Discount,
		req.PaymentType,
		req.Status,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *orderRepo) Update(ctx context.Context, req *order_service.Order) (string, error) {

	query := `
	UPDATE
		orders
	SET
		order_id=$2,client_id=$3,branch_id=$4,courier_id=$5,type=$6,address=$7,delivery_price=$8,price=$9,discount=$10,payment_type=$11,status=$12,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.OrderId,
		req.ClientId,
		req.BranchId,
		req.CourierId,
		req.Type,
		req.Address,
		req.DeliveryPrice,
		req.Price,
		req.Discount,
		req.PaymentType,
		req.Status,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *orderRepo) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.Order, error) {

	query := `
	SELECT
	id,
	order_id,
	client_id,
	branch_id,
	courier_id,
	type,
	address,
	delivery_price,
	price,
	discount,
	payment_type,
	status,
	created_at::text,
	updated_at::text
	FROM orders
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var order order_service.Order

	err := resp.Scan(
		&order.Id,
		&order.OrderId,
		&order.ClientId,
		&order.BranchId,
		&order.CourierId,
		&order.Type,
		&order.Address,
		&order.DeliveryPrice,
		&order.Price,
		&order.Discount,
		&order.PaymentType,
		&order.Status,
		&order.CreatedAt,
		&order.UpdatedAt,
	)

	if err != nil {
		return &order_service.Order{}, err
	}

	return &order, nil
}

func (c *orderRepo) Delete(ctx context.Context, req *order_service.IdReqRes) (string, error) {

	query := `UPDATE orders SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *orderRepo) GetAll(ctx context.Context, req *order_service.GetAllOrderRequest) (resp *order_service.GetAllOrderResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	order_id,
	client_id,
	branch_id,
	courier_id,
	type,
	address,
	delivery_price,
	price,
	discount,
	payment_type,
	status,
	created_at::text,
	updated_at::text
	FROM orders
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		orders
	`

	if req.GetType() != "" {
		filter += ` AND type ILIKE ` + "'%" + req.GetType() + "%'"
	}

	if req.GetPaymentType() != "" {
		filter += ` AND payment_type ILIKE ` + "'%" + req.GetPaymentType() + "%'"
	}

	if req.GetOrderId() != "" {
		filter += ` AND order_id='` + req.GetOrderId() + `' `
	}

	if req.GetClientId() != "" {
		filter += ` AND client_id='` + req.GetClientId() + `' `
	}

	if req.GetBranchId() != "" {
		filter += ` AND branch_id='` + req.GetBranchId() + `' `
	}

	if req.GetCourierId() != "" {
		filter += ` AND courier_id='` + req.GetCourierId() + `' `
	}

	filter += ` AND price BETWEEN ` + fmt.Sprintf("%f", req.GetFromPrice()) + ` AND ` + fmt.Sprintf("%f", req.GetToPrice())

	if req.GetLimit() > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &order_service.GetAllOrderResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &order_service.GetAllOrderResponse{}, err
	}

	defer rows.Close()

	result := []*order_service.Order{}

	for rows.Next() {

		var order order_service.Order

		err := rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.ClientId,
			&order.BranchId,
			&order.CourierId,
			&order.Type,
			&order.Address,
			&order.DeliveryPrice,
			&order.Price,
			&order.Discount,
			&order.PaymentType,
			&order.Status,
			&order.CreatedAt,
			&order.UpdatedAt,
		)
		if err != nil {
			return &order_service.GetAllOrderResponse{}, err
		}

		result = append(result, &order)

	}

	return &order_service.GetAllOrderResponse{Orders: result, Count: int64(count)}, nil

}

func (c *orderRepo) UpdateStatus(ctx context.Context, req *order_service.UpdateOrderStatus) (string, error) {

	query := `
	UPDATE
		orders
	SET
		status=$2,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.Status,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *orderRepo) GetAllNotCourierAcceptedOrder(ctx context.Context, req *order_service.PageLimit) (resp *order_service.GetAllOrderResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND courier_id='' AND status='accepted' "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	order_id,
	client_id,
	branch_id,
	courier_id,
	type,
	address,
	delivery_price,
	price,
	discount,
	payment_type,
	status,
	created_at::text,
	updated_at::text
	FROM orders
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		orders
	`

	if req.GetLimit() > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &order_service.GetAllOrderResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &order_service.GetAllOrderResponse{}, err
	}

	defer rows.Close()

	result := []*order_service.Order{}

	for rows.Next() {

		var order order_service.Order

		err := rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.ClientId,
			&order.BranchId,
			&order.CourierId,
			&order.Type,
			&order.Address,
			&order.DeliveryPrice,
			&order.Price,
			&order.Discount,
			&order.PaymentType,
			&order.Status,
			&order.CreatedAt,
			&order.UpdatedAt,
		)
		if err != nil {
			return &order_service.GetAllOrderResponse{}, err
		}

		result = append(result, &order)

	}

	return &order_service.GetAllOrderResponse{Orders: result, Count: int64(count)}, nil

}

func (c *orderRepo) GetAllByCourierId(ctx context.Context, req *order_service.CourierIdRequest) (resp *order_service.GetAllOrderResponse, err error) {

	var (
		filter = " WHERE deleted_at IS NULL AND status='courier_accepted' OR status='ready_in_branch' OR status='on_way' "
	)

	info := `
	SELECT
	id,
	order_id,
	client_id,
	branch_id,
	courier_id,
	type,
	address,
	delivery_price,
	price,
	discount,
	payment_type,
	status,
	created_at::text,
	updated_at::text
	FROM orders
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		orders
	`

	if req.GetCourierId() != "" {
		filter += ` AND courier_id='` + req.GetCourierId() + `' `
	}

	query := info + filter
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &order_service.GetAllOrderResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &order_service.GetAllOrderResponse{}, err
	}

	defer rows.Close()

	result := []*order_service.Order{}

	for rows.Next() {

		var order order_service.Order

		err := rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.ClientId,
			&order.BranchId,
			&order.CourierId,
			&order.Type,
			&order.Address,
			&order.DeliveryPrice,
			&order.Price,
			&order.Discount,
			&order.PaymentType,
			&order.Status,
			&order.CreatedAt,
			&order.UpdatedAt,
		)
		if err != nil {
			return &order_service.GetAllOrderResponse{}, err
		}

		result = append(result, &order)

	}

	return &order_service.GetAllOrderResponse{Orders: result, Count: int64(count)}, nil

}
