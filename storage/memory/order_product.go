package memory

import (
	"context"
	"fmt"
	"order/genproto/order_service"
	"order/packages/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type orderProductRepo struct {
	db *pgxpool.Pool
}

func NewOrderProductRepo(db *pgxpool.Pool) *orderProductRepo {

	return &orderProductRepo{
		db: db,
	}

}

func (c *orderProductRepo) Create(ctx context.Context, req *order_service.CreateOrderProduct) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
		order_products (id,order_id,product_id,quantity,price)
	VALUES ($1,$2,$3,$4,$5)`

	_, err := c.db.Exec(ctx, query,
		id,
		req.OrderId,
		req.ProductId,
		req.Quantity,
		req.Price,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *orderProductRepo) Update(ctx context.Context, req *order_service.OrderProduct) (string, error) {

	query := `
	UPDATE
		order_products
	SET
		order_id=$2,product_id=$3,quantity=$4,price=$5,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.OrderId,
		req.ProductId,
		req.Quantity,
		req.Price,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *orderProductRepo) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.OrderProduct, error) {

	query := `
	SELECT
	id,
	order_id,
	product_id,
	quantity,
	price,
	created_at::text,
	updated_at::text
	FROM order_products
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var order order_service.OrderProduct

	err := resp.Scan(
		&order.Id,
		&order.OrderId,
		&order.ProductId,
		&order.Quantity,
		&order.Price,
		&order.CreatedAt,
		&order.UpdatedAt,
	)

	if err != nil {
		return &order_service.OrderProduct{}, err
	}

	return &order, nil
}

func (c *orderProductRepo) Delete(ctx context.Context, req *order_service.IdReqRes) (string, error) {

	query := `UPDATE order_products SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *orderProductRepo) GetAll(ctx context.Context, req *order_service.GetAllOrderProductRequest) (resp *order_service.GetAllOrderProductResponse, err error) {

	var (
		params  = make(map[string]interface{})
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	order_id,
	product_id,
	quantity,
	price,
	created_at::text,
	updated_at::text
	FROM order_products
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		order_products
	`

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	q, pArr := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, q, pArr...)
	if err != nil {
		return &order_service.GetAllOrderProductResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &order_service.GetAllOrderProductResponse{}, err
	}

	defer rows.Close()

	result := []*order_service.OrderProduct{}

	for rows.Next() {

		var order order_service.OrderProduct

		err := rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.ProductId,
			&order.Quantity,
			&order.Price,
			&order.CreatedAt,
			&order.UpdatedAt,
		)
		if err != nil {
			return &order_service.GetAllOrderProductResponse{}, err
		}

		result = append(result, &order)

	}

	return &order_service.GetAllOrderProductResponse{OrderProducts: result, Count: int64(count)}, nil

}
