package memory

import (
	"context"
	"fmt"
	"order/config"
	"order/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type store struct {
	db                *pgxpool.Pool
	orders            *orderRepo
	orderProducts     *orderProductRepo
	deliveryTarifs    *deliveryTarifRepo
	alternativeTarifs *alternativeTarifRepo
}

func NewStorage(ctx context.Context, cfg config.Config) (storage.StoregeI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		fmt.Println("ParseConfig:", err.Error())
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		fmt.Println("ConnectConfig:", err.Error())
		return nil, err
	}
	return &store{
		db: pool,
	}, nil
}

func (s *store) Order() storage.OrderI {
	if s.orders == nil {
		s.orders = NewOrderRepo(s.db)
	}
	return s.orders
}

func (s *store) OrderProduct() storage.OrderProductI {
	if s.orderProducts == nil {
		s.orderProducts = NewOrderProductRepo(s.db)
	}
	return s.orderProducts
}

func (s *store) DeliveryTarif() storage.DeliveryTarifI {
	if s.deliveryTarifs == nil {
		s.deliveryTarifs = NewDeliveryTarifRepo(s.db)
	}
	return s.deliveryTarifs
}

func (s *store) AlternativeTarif() storage.AlternativeTarifI {
	if s.alternativeTarifs == nil {
		s.alternativeTarifs = NewAlternativeTarifRepo(s.db)
	}
	return s.alternativeTarifs
}
