package memory

import (
	"context"
	"fmt"
	"order/genproto/order_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type alternativeTarifRepo struct {
	db *pgxpool.Pool
}

func NewAlternativeTarifRepo(db *pgxpool.Pool) *alternativeTarifRepo {

	return &alternativeTarifRepo{
		db: db,
	}

}

func (c *alternativeTarifRepo) Create(ctx context.Context, req *order_service.CreateAlternativeTarif) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
		alternative_tarif_values (id,delivery_tarif_id,from_price,to_price,price)
	VALUES ($1,$2,$3,$4,$5)`

	_, err := c.db.Exec(ctx, query,
		id,
		req.DeliveryTarifId,
		req.FromPrice,
		req.ToPrice,
		req.Price,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *alternativeTarifRepo) Update(ctx context.Context, req *order_service.AlternativeTarif) (string, error) {

	query := `
	UPDATE
		alternative_tarif_values
	SET
		delivery_tarif_id=$2,from_price=$3,to_price=$4,price=$5,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.DeliveryTarifId,
		req.FromPrice,
		req.ToPrice,
		req.Price,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *alternativeTarifRepo) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.AlternativeTarif, error) {

	query := `
	SELECT
	id,
	delivery_tarif_id,
	from_price,
	to_price,
	price,
	created_at::text,
	updated_at::text
	FROM alternative_tarif_values
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var tarif order_service.AlternativeTarif

	err := resp.Scan(
		&tarif.Id,
		&tarif.DeliveryTarifId,
		&tarif.FromPrice,
		&tarif.ToPrice,
		&tarif.Price,
		&tarif.CreatedAt,
		&tarif.UpdatedAt,
	)

	if err != nil {
		return &order_service.AlternativeTarif{}, err
	}

	return &tarif, nil
}

func (c *alternativeTarifRepo) Delete(ctx context.Context, req *order_service.IdReqRes) (string, error) {

	query := `UPDATE alternative_tarif_values SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *alternativeTarifRepo) GetAll(ctx context.Context, req *order_service.GetAllAlternativeTarifRequest) (resp *order_service.GetAllAlternativeTarifResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	delivery_tarif_id,
	from_price,
	to_price,
	price,
	created_at::text,
	updated_at::text
	FROM alternative_tarif_values
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		alternative_tarif_values
	`
	if req.GetDeliveryTarifId() != "" {
		filter += ` AND delivery_tarif_id='` + req.GetDeliveryTarifId() + `' `
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &order_service.GetAllAlternativeTarifResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &order_service.GetAllAlternativeTarifResponse{}, err
	}

	defer rows.Close()

	result := []*order_service.AlternativeTarif{}

	for rows.Next() {

		var tarif order_service.AlternativeTarif

		err := rows.Scan(
			&tarif.Id,
			&tarif.DeliveryTarifId,
			&tarif.FromPrice,
			&tarif.ToPrice,
			&tarif.Price,
			&tarif.CreatedAt,
			&tarif.UpdatedAt,
		)
		if err != nil {
			return &order_service.GetAllAlternativeTarifResponse{}, err
		}

		result = append(result, &tarif)

	}

	return &order_service.GetAllAlternativeTarifResponse{AlternativeTarifs: result, Count: int64(count)}, nil

}
