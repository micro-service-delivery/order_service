package memory

import (
	"context"
	"fmt"
	"order/genproto/order_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type deliveryTarifRepo struct {
	db *pgxpool.Pool
}

func NewDeliveryTarifRepo(db *pgxpool.Pool) *deliveryTarifRepo {

	return &deliveryTarifRepo{
		db: db,
	}

}

func (c *deliveryTarifRepo) Create(ctx context.Context, req *order_service.CreateDeliveryTarif) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
		delivery_tarifs (id,name,type,base_price)
	VALUES ($1,$2,$3,$4)`

	_, err := c.db.Exec(ctx, query,
		id,
		req.Name,
		req.Type,
		req.BasePrice,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *deliveryTarifRepo) Update(ctx context.Context, req *order_service.DeliveryTarif) (string, error) {

	query := `
	UPDATE
		delivery_tarifs
	SET
		name=$2,type=$3,base_price=$4,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.Name,
		req.Type,
		req.BasePrice,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *deliveryTarifRepo) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.DeliveryTarif, error) {

	query := `
	SELECT
	id,
	name,
	type,
	base_price,
	created_at::text,
	updated_at::text
	FROM delivery_tarifs
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var tarif order_service.DeliveryTarif

	err := resp.Scan(
		&tarif.Id,
		&tarif.Name,
		&tarif.Type,
		&tarif.BasePrice,
		&tarif.CreatedAt,
		&tarif.UpdatedAt,
	)

	if err != nil {
		return &order_service.DeliveryTarif{}, err
	}

	return &tarif, nil
}

func (c *deliveryTarifRepo) Delete(ctx context.Context, req *order_service.IdReqRes) (string, error) {

	query := `UPDATE delivery_tarifs SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *deliveryTarifRepo) GetAll(ctx context.Context, req *order_service.GetAllDeliveryTarifRequest) (resp *order_service.GetAllDeliveryTarifResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	name,
	type,
	base_price,
	created_at::text,
	updated_at::text
	FROM delivery_tarifs
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		delivery_tarifs
	`
	if req.GetName() != "" {
		filter += ` AND name ILIKE ` + "'%" + req.GetName() + "%'"
	}

	if req.GetType() != "" {
		filter += ` AND type ILIKE ` + "'%" + req.GetType() + "%'"
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &order_service.GetAllDeliveryTarifResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &order_service.GetAllDeliveryTarifResponse{}, err
	}

	defer rows.Close()

	result := []*order_service.DeliveryTarif{}

	for rows.Next() {

		var tarif order_service.DeliveryTarif

		err := rows.Scan(
			&tarif.Id,
			&tarif.Name,
			&tarif.Type,
			&tarif.BasePrice,
			&tarif.CreatedAt,
			&tarif.UpdatedAt,
		)
		if err != nil {
			return &order_service.GetAllDeliveryTarifResponse{}, err
		}

		result = append(result, &tarif)

	}

	return &order_service.GetAllDeliveryTarifResponse{DeliveryTarifs: result, Count: int64(count)}, nil

}
