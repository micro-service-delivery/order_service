package service

import (
	"context"
	"order/genproto/order_service"
	grpc_client "order/grpc/client"
	"order/packages/logger"
	"order/storage"
)

type OrderProductService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	order_service.UnimplementedOrderProductServiceServer
}

func NewOrderProductService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *OrderProductService {
	return &OrderProductService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *OrderProductService) Create(ctx context.Context, req *order_service.CreateOrderProduct) (*order_service.IdReqRes, error) {
	id, err := t.storage.OrderProduct().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.IdReqRes{Id: id}, nil
}

func (t *OrderProductService) Update(ctx context.Context, req *order_service.OrderProduct) (*order_service.ResponseString, error) {
	str, err := t.storage.OrderProduct().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: str}, nil
}

func (t *OrderProductService) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.OrderProduct, error) {
	OrderProduct, err := t.storage.OrderProduct().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return OrderProduct, nil
}

func (t *OrderProductService) GetAll(ctx context.Context, req *order_service.GetAllOrderProductRequest) (*order_service.GetAllOrderProductResponse, error) {
	OrderProducts, err := t.storage.OrderProduct().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return OrderProducts, nil
}

func (t *OrderProductService) Delete(ctx context.Context, req *order_service.IdReqRes) (*order_service.ResponseString, error) {
	text, err := t.storage.OrderProduct().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: text}, nil
}
