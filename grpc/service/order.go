package service

import (
	"order/genproto/order_service"
	grpc_client "order/grpc/client"

	"context"
	"order/packages/logger"
	"order/storage"
)

type OrderService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	order_service.UnimplementedOrderServiceServer
}

func NewOrderService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *OrderService {
	return &OrderService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *OrderService) Create(ctx context.Context, req *order_service.CreateOrder) (*order_service.IdReqRes, error) {
	id, err := t.storage.Order().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.IdReqRes{Id: id}, nil
}

func (t *OrderService) Update(ctx context.Context, req *order_service.Order) (*order_service.ResponseString, error) {
	str, err := t.storage.Order().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: str}, nil
}

func (t *OrderService) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.Order, error) {
	order, err := t.storage.Order().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return order, nil
}

func (t *OrderService) GetAll(ctx context.Context, req *order_service.GetAllOrderRequest) (*order_service.GetAllOrderResponse, error) {
	orders, err := t.storage.Order().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return orders, nil
}

func (t *OrderService) Delete(ctx context.Context, req *order_service.IdReqRes) (*order_service.ResponseString, error) {
	text, err := t.storage.Order().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: text}, nil
}

func (t *OrderService) UpdateStatus(ctx context.Context, req *order_service.UpdateOrderStatus) (*order_service.ResponseString, error) {
	str, err := t.storage.Order().UpdateStatus(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: str}, nil
}

func (t *OrderService) GetAllNotCourierAcceptedOrder(ctx context.Context, req *order_service.PageLimit) (*order_service.GetAllOrderResponse, error) {
	orders, err := t.storage.Order().GetAllNotCourierAcceptedOrder(ctx, req)
	if err != nil {
		return nil, err
	}

	return orders, nil
}

func (t *OrderService) GetAllByCourierId(ctx context.Context, req *order_service.CourierIdRequest) (*order_service.GetAllOrderResponse, error) {
	orders, err := t.storage.Order().GetAllByCourierId(ctx, req)
	if err != nil {
		return nil, err
	}

	return orders, nil
}
