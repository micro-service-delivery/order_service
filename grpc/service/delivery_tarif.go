package service

import (
	"context"
	"order/genproto/order_service"
	grpc_client "order/grpc/client"
	"order/packages/logger"
	"order/storage"
)

type DeliveryTarifService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	order_service.UnimplementedDeliveryTarifServiceServer
}

func NewDeliveryTarifService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *DeliveryTarifService {
	return &DeliveryTarifService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *DeliveryTarifService) Create(ctx context.Context, req *order_service.CreateDeliveryTarif) (*order_service.IdReqRes, error) {
	id, err := t.storage.DeliveryTarif().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.IdReqRes{Id: id}, nil
}

func (t *DeliveryTarifService) Update(ctx context.Context, req *order_service.DeliveryTarif) (*order_service.ResponseString, error) {
	str, err := t.storage.DeliveryTarif().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: str}, nil
}

func (t *DeliveryTarifService) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.DeliveryTarif, error) {
	deliveryTarif, err := t.storage.DeliveryTarif().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return deliveryTarif, nil
}

func (t *DeliveryTarifService) GetAll(ctx context.Context, req *order_service.GetAllDeliveryTarifRequest) (*order_service.GetAllDeliveryTarifResponse, error) {
	deliveryTarifs, err := t.storage.DeliveryTarif().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return deliveryTarifs, nil
}

func (t *DeliveryTarifService) Delete(ctx context.Context, req *order_service.IdReqRes) (*order_service.ResponseString, error) {
	text, err := t.storage.DeliveryTarif().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: text}, nil
}
