package service

import (
	"context"
	"order/genproto/order_service"
	grpc_client "order/grpc/client"
	"order/packages/logger"
	"order/storage"
)

type AlternativeTarifService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	order_service.UnimplementedAlternativeTarifServiceServer
}

func NewAlternativeTarifService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *AlternativeTarifService {
	return &AlternativeTarifService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *AlternativeTarifService) Create(ctx context.Context, req *order_service.CreateAlternativeTarif) (*order_service.IdReqRes, error) {
	id, err := t.storage.AlternativeTarif().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.IdReqRes{Id: id}, nil
}

func (t *AlternativeTarifService) Update(ctx context.Context, req *order_service.AlternativeTarif) (*order_service.ResponseString, error) {
	str, err := t.storage.AlternativeTarif().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: str}, nil
}

func (t *AlternativeTarifService) Get(ctx context.Context, req *order_service.IdReqRes) (*order_service.AlternativeTarif, error) {
	alternativeTarif, err := t.storage.AlternativeTarif().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return alternativeTarif, nil
}

func (t *AlternativeTarifService) GetAll(ctx context.Context, req *order_service.GetAllAlternativeTarifRequest) (*order_service.GetAllAlternativeTarifResponse, error) {
	alternativeTarifs, err := t.storage.AlternativeTarif().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return alternativeTarifs, nil
}

func (t *AlternativeTarifService) Delete(ctx context.Context, req *order_service.IdReqRes) (*order_service.ResponseString, error) {
	text, err := t.storage.AlternativeTarif().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &order_service.ResponseString{Text: text}, nil
}
