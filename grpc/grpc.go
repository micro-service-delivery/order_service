package grpc

import (
	"order/genproto/order_service"
	grpc_client "order/grpc/client"
	"order/grpc/service"
	"order/packages/logger"
	"order/storage"

	"google.golang.org/grpc"
)

func SetUpServer(log logger.LoggerI, strg storage.StoregeI, grpcClient grpc_client.GrpcClientI) *grpc.Server {
	s := grpc.NewServer()
	order_service.RegisterOrderServiceServer(s, service.NewOrderService(log, strg, grpcClient))
	order_service.RegisterOrderProductServiceServer(s, service.NewOrderProductService(log, strg, grpcClient))
	order_service.RegisterDeliveryTarifServiceServer(s, service.NewDeliveryTarifService(log, strg, grpcClient))
	order_service.RegisterAlternativeTarifServiceServer(s, service.NewAlternativeTarifService(log, strg, grpcClient))
	return s
}
