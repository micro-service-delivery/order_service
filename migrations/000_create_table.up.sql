CREATE TYPE "type_delivery_tarif" AS ENUM (
  'fixed',
  'alternative'
);

CREATE TYPE "type_discount" AS ENUM (
  'fixed',
  'percent'
);

CREATE TYPE "type_order" AS ENUM (
  'delivery',
  'pickUp'
);

CREATE TYPE "type_status" AS ENUM (
  'accepted',
  'courier_accepted',
  'ready_in_branch',
  'on_way',
  'finished',
  'canceled'
);

CREATE TYPE "type_payment" AS ENUM (
  'card',
  'cash'
);

CREATE TABLE "orders" (
  "id" uuid PRIMARY KEY,
  "order_id" varchar UNIQUE,
  "client_id" uuid,
  "branch_id" uuid,
  "courier_id" uuid,
  "type" type_order,
  "address" varchar,
  "delivery_price" numeric,
  "price" numeric,
  "discount" numeric,
  "payment_type" type_payment,
  "status" type_status,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);


CREATE TABLE "order_products" (
  "id" uuid PRIMARY KEY,
  "order_id" uuid,
  "product_id" uuid,
  "quantity" integer,
  "price" numeric,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "delivery_tarifs" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "type" type_delivery_tarif,
  "base_price" numeric,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "alternative_tarif_values" (
  "id" uuid PRIMARY KEY,
  "delivery_tarif_id" uuid,
  "from_price" numeric,
  "to_price" numeric,
  "price" numeric,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);


CREATE OR REPLACE FUNCTION generate_order_id() RETURNS TRIGGER AS $$
DECLARE
  next_id text;
BEGIN
  SELECT lpad(CAST(COALESCE(MAX(order_id::integer), 0) + 1 AS text), 6, '0') INTO next_id FROM "orders";
  
  NEW.order_id := next_id;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER generate_order_id_trigger
BEFORE INSERT ON "orders"
FOR EACH ROW
EXECUTE FUNCTION generate_order_id();

ALTER TABLE "alternative_tarif_values" ADD FOREIGN KEY ("delivery_tarif_id") REFERENCES "delivery_tarifs" ("id");

ALTER TABLE "order_products" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");